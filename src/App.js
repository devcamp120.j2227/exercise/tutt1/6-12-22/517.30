import './App.css';
import "bootstrap/dist/css/bootstrap.min.css"
import th from "./assets/images/1.webp"
import Content from './components/content/contentComponent';
import TextHeader from './components/headers/text/textHeader';
function App() {
  return (
    <div className='container '>
      <TextHeader/>
      <div className='row mt-5'>
        <img src={th} alt="th" style={{ width: "500px" }}></img>
      </div>
      <Content/>
    </div>
  );
}

export default App;
